# Example #


#### Software development stack ####
- Traefik 1.7.21
- Redis 5.0.5
- FastApi 0.48.0

#### Deploy ####
```
git clone git@bitbucket.org:mikibouns/example.git
cd example
sudo docker-compose up --build
```

#### URLs ####

http://api.localhost/api/docs#/ (API documentation)

http://traefik.localhost/dashboard/ (Traefik dashboard)

#### Credentials ####

Login: **admin**

Password: **AdminPassword**

#### Tests

Use **pytest**
> run tests only with running **Redis**

