from loguru import logger
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.utils import get_openapi
from fastapi import Depends
from fastapi import APIRouter
from starlette.responses import JSONResponse

from app.security import basic_auth_validation
from app.config import WEBAPP_NAME, API_VERSION
from app import app


router = APIRouter()


@router.get("/docs")
async def get_api_docs(current_user: str = Depends(basic_auth_validation)):
    return get_swagger_ui_html(openapi_url="/api/openapi.json", title="docs")


@router.get("/openapi.json")
async def get_api_docs(current_user: str = Depends(basic_auth_validation)):
    return JSONResponse(get_openapi(title=WEBAPP_NAME, version=API_VERSION, routes=app.routes))