from loguru import logger
from fastapi import APIRouter, Body
from typing import List
from urllib.parse import urlparse
from datetime import datetime

from app import redis


router = APIRouter()


@router.post("/visited_links")
async def visited_links(links: List[str] = Body(..., embed=True)):
    """Set link list"""
    logger.info('### Set link list')
    creation_date = int(datetime.utcnow().timestamp())
    if links:
        answer = redis.lpush(str(creation_date), *links)
        logger.info(answer)
        return {"status": "ok"}
    return {"status": "no data"}


@router.get("/visited_domains")
async def visited_domains(from_: int = 0, to_: int = 0):
    """Get visited domains"""
    logger.info('### Get visited domains')
    result = set()
    keys = [key for key in redis.keys() if from_ <= int(key) <= to_]
    if keys:
        for key in keys:
            for url in redis.lrange(key, 0, -1):
                url = urlparse(url)
                result.add(url.netloc if url.netloc else url.path)

    response = {
        'domains': list(result),
        'status': 'ok'
    }
    return response