import secrets

from starlette.status import HTTP_401_UNAUTHORIZED
from fastapi import Depends, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from app.config import SECRET, API_DOCS_USER, API_DOCS_PASSWD


security = HTTPBasic()


def basic_auth_validation(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, API_DOCS_USER)
    correct_password = secrets.compare_digest(credentials.password, API_DOCS_PASSWD)
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username
