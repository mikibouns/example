from fastapi import FastAPI
from starlette.responses import JSONResponse
from starlette import status
from starlette.requests import Request
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError

from app.db.redis import init_redis
from app.config import WEBAPP_NAME, API_VERSION


app = FastAPI(title=WEBAPP_NAME,
              openapi_url=None,
              docs_url=None,
              redoc_url=None,
              version=API_VERSION)

redis = init_redis()


# override exception handler
@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder({"status": exc.errors(), }),
    )


@app.on_event('startup')
async def startup_event():
    pass


@app.on_event('shutdown')
async def shutdown_event():
    pass