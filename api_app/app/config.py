import os

# main config
WEBAPP_NAME = os.getenv('WEBAPP_NAME', 'FastAPI')
API_VERSION = os.getenv('API_VERSION', '0.1.0')
API_DOCS_USER = os.getenv('API_DOCS_USER', 'admin')
API_DOCS_PASSWD = os.getenv('API_DOCS_PASSWD', 'AdminPassword')

# auth config
SECRET = os.getenv('SECRET', 'secret')
SALT = os.getenv('SALT', 'salt')

# redis config
REDIS_HOST = os.getenv('REDIS_HOST', '127.0.0.1')
REDIS_PORT = int(os.getenv('REDIS_PORT', '6379'))
REDIS_NAME = int(os.getenv('REDIS_NAME', '1'))