from starlette.staticfiles import StaticFiles
from loguru import logger

from app import *
from app.routers import api, main_route


app.mount("/static", StaticFiles(directory="static"), name="static")

app.include_router(api.router, prefix="/api", tags=["api docs"])
app.include_router(main_route.router, prefix="", tags=["api docs"])


