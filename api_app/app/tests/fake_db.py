import random
import fakeredis


urls = [
        "https://ya.ru",
        "https://ya.ru?q=123",
        "funbox.ru",
        "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
    ]

fake_redis = fakeredis.FakeStrictRedis()

# fill database
for key in [random.randrange(1587772800, 1588377600) for _ in range(10)]:
    fake_redis.lpush(key, *random.sample(urls, random.randrange(1, len(urls))))


if __name__ == '__main__':
    pass