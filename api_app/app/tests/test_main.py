from starlette.testclient import TestClient
from urllib.parse import urlparse

from app import redis
from app.main import *
from .fake_db import fake_redis


client = TestClient(app)

app.dependency_overrides[redis] = fake_redis


def test_visited_links():
    request_data = [
        {
            'links': [
                "https://ya.ru",
                "https://ya.ru?q=123",
                "funbox.ru",
                "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
            ]
        },
        {
            'links': [
                "ya.ru",
                "https://ya.ru?q=123"
            ]
        },
        {
            'links': []
        }
    ]
    for data in request_data:
        response = client.post(
            "/visited_links",
            json=data
        )
        if not data['links']:
            assert response.status_code == 200
            assert response.json() == {"status": "no data"}
        else:
            assert response.status_code == 200
            assert response.json() == {"status": "ok"}


def test_visited_domains():
    request_data = [
        {'from_': 1588377600, 'to_': 1587772800},
        {'from_': 1588204800, 'to_': 1588636800},
        {'from_': 1585699200, 'to_': 1588118400}
    ]
    for data in request_data:
        db_data = [key for key in redis.keys() if data['from_'] <= int(key) <= data['to_']]
        data_list = set()
        for key in db_data:
            for url in redis.lrange(key, 0, -1):
                url = urlparse(url)
                data_list.add((url.netloc if url.netloc else url.path).decode())

        response = client.get(
            "/visited_domains",
            params=data
        )
        assert response.status_code == 200
        assert response.json() == {
            "domains": list(data_list),
            'status': 'ok'
        }