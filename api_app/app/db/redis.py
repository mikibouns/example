import redis

from app.config import (
    REDIS_HOST,
    REDIS_PORT,
    REDIS_NAME
)


def init_redis():
    return redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_NAME)